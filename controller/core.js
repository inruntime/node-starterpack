/*
 * Core Controller
 */

module.exports = function(app) {

	var approot = GLOBAL.approot;

	/* ----==== ROUTES ====---- */

	app.get('/', function(req, res) {
		// render a template from /views/core/frontpage with EJS
		res.render('core/frontpage');
	});

	app.get('/pages/:id', function(req, res){
		// output JSON result
		res.jsonp({result:"this shows page"+req.params.id})
	});
};