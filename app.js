/* TIMEZONE */

process.env.TZ = 'Europe/London';
//process.env.NODE_DEBUG = true;

/* INCLUDES */

var fs = require('fs'),
	express = require('express');

/* CONFIG */

GLOBAL.cfg = require('./config.js');

/* PORT */

var port = process.env.PORT || 5000;

/* EXPRESS */

var app = express();

/*  */

/* APP ROOT FOLDER */

var approot = GLOBAL.approot = __dirname;

/* BRAINS */

require(approot+'/controller/init.js')(app, express, function() {

	// require your models and controllers here (models first, controllers later)
	// require(approot+'/models/modelname.js')(app);
	// require(approot+'/controller/controllername.js')(app);

	/* mongoose models can auto register under app.models.<modelname>, like so:
		
		exports = module.exports = function(app) {
		  var TestSchema = new Schema({
		      created         : { type: Date, default: Date.now}
		    , name            : { type: String, default: 'Demo'}
		    , version  		  : { type: String, default: '1.0.0'}
		  }, { 
		    safe: true, 
		    strict: "throw" 
		  });
		  app.models.Test = app.services.mongoose.model('Test', TestSchema);
		};
	*/

	/* 
		models will then be available anywhere in the application through app.models.<modelname> 
		
		var Test = app.models.Test;
		var t = new Test({name: "Bob",version: "2.0.0"});
	*/
	
	require(approot+'/controller/core.js')(app);

	// LISTEN TO REQUESTS

	app.listen(port, function() {
	  console.log("[STATUS: OK] Listening on " + port);
	});

});