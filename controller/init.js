module.exports = function(app, express, cb) {

	var approot = GLOBAL.approot;

	var mongoose = require('mongoose');
	//app.services.mongoose.set('debug', true);

	mongoose.connection.on("open", function(){ 

		console.log("[STATUS: OK] MongoDB is connected!"); 

		/* CREATE NAMESPACES */

		app.services = {};
    	app.operations = {};
    	app.models = {};
    	app.utils = {};

		/* RENDER ENGINE & VIEWS */

		app.engine('ejs', require('ejs-locals'));
		app.set("views", approot + '/views');
		app.set('view engine', 'ejs');

		/* EXPRESS init */

		app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
		app.use(express.static(approot + '/cdn'));
		app.use(express.cookieParser(GLOBAL.cfg.cookie_secret));
	  	app.use(express.bodyParser());
	  	app.use(express.methodOverride());
		app.use(express.session({
			secret: GLOBAL.cfg.cookie_secret,
			cookie: { maxAge: 86400000 }
		}));

		/* ROUTER */

		app.use(app.router);

		/* DONE! */

		return cb();
	});

	var db = mongoose.connect(GLOBAL.cfg.db_url, {db: { safe: true }, user: GLOBAL.cfg.db_user, pass: GLOBAL.cfg.db_pass} , function(err) { 
		if (err) console.log(err);
	});

}